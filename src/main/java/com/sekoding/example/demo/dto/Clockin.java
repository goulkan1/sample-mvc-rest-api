package com.sekoding.example.demo.dto;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Clockin {
    private String[] location;
    private Date starttime;
    private String description;
    private String picture;

}
