package com.sekoding.example.demo.controller;

import javax.validation.Valid;

import com.sekoding.example.demo.dto.ResponseData;
import com.sekoding.example.demo.models.entities.Absen;
import com.sekoding.example.demo.models.repos.AbsenRepo;
import com.sekoding.example.demo.services.AbsenService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/absen")
public class AbsenController {

    @Autowired
    AbsenService absenService;

    @PostMapping("/clockin")
    public ResponseEntity<ResponseData<Absen>> register(@Valid @RequestBody Absen absen, Errors errors) {
        ResponseData<Absen> responseData = new ResponseData<>();

        if (errors.hasErrors()) {
            for (ObjectError error : errors.getAllErrors()) {
                responseData.getMessage().add((error.getDefaultMessage()));
            }
            responseData.setStatus(false);
            responseData.setPayload(null);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseData);
        }

        responseData.setStatus(true);
        responseData.setPayload(absenService.save(absen));
        return ResponseEntity.ok(responseData);

    }
}
