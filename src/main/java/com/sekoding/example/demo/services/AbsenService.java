package com.sekoding.example.demo.services;

import javax.transaction.Transactional;

import com.sekoding.example.demo.models.entities.Absen;
import com.sekoding.example.demo.models.repos.AbsenRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class AbsenService {

    @Autowired
    private AbsenRepo absenRepo;

    public Absen save(Absen absen) {
        return absenRepo.save(absen);
    }

}
