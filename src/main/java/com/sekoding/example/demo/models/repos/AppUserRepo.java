package com.sekoding.example.demo.models.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

import com.sekoding.example.demo.models.entities.AppUser;

public interface AppUserRepo extends JpaRepository<AppUser, Long> {
    Optional<AppUser> findByUsername(String username);
}
