package com.sekoding.example.demo.models.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "tbl_absen")
@Getter
@Setter
public class Absen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(length = 150, nullable = false)
    private String[] location;
    @Column(nullable = false)
    private Date starttime;
    @Column(nullable = false)
    private Date endtime;
    @Column(length = 200, nullable = false)
    private String description;
    @Column(length = 200, nullable = false)
    private String picture;

    @ManyToOne
    private AppUser AppUserId;
}
