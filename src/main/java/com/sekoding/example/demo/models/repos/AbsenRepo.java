package com.sekoding.example.demo.models.repos;

import com.sekoding.example.demo.models.entities.Absen;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AbsenRepo extends JpaRepository<Absen, Long> {

}
